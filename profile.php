<?php
	include 'lib/User.php';
	include 'inc/header.php';
	Session::checkSession();
?>

<?php 
	if (isset($_GET['id'])){
		$userid =  (int)$_GET['id'];
	}
	$user = new User();
	if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update'])) {
	 	$updateusr = $user->updateUserData($userid, $_POST);
	 }
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h2>User Profile <span class="pull-right"><a href="index.php" class="btn btn-primary">Back</a></span> </h2>
	</div>

	<div class="panel-body">
		<div style="max-width: 600px; margin: 0 auto;">
<?php 
	if (isset($updateusr)) {
		echo $updateusr;
	}
?>
<?php 
	$userdata = $user->getUserById($userid);
	if ($userdata) {
 ?>		
			<form action="" method="POST">
				<div class="form-group">
					<label for="name">Name</label>
					<input type="text" name="name" id="name" class="form-control" value="<?php echo $userdata->name; ?>" placeholder="Enter Your Name">
				</div>

				<div class="form-group">
					<label for="email">Username</label>
					<input type="text" name="username" id="username" class="form-control" value="<?php echo $userdata->username; ?>" placeholder="Enter Your Username">
				</div>

				<div class="form-group">
					<label for="email">Email Address</label>
					<input type="text" name="email" id="email" class="form-control" value="<?php echo $userdata->email; ?>" " placeholder="Enter Your Email Addressddress">
				</div>
				<?php 
					$sesId = Session::get("id");
					if ($userid == $sesId){
				?>
				<button class="btn btn-success" type="submit" name="update">Update</button>
				<a class="btn btn-info" href="changepass.php?id=<?php echo $userid; ?>">Password Change</a>
				<?php } ?>
			</form>
<?php } ?>	
		</div>
	</div>
</div>
<?php
	include 'inc/footer.php';
?>
		