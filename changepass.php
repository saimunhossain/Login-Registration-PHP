<?php
	include 'lib/User.php';
	include 'inc/header.php';
	Session::checkSession();
?>

<?php 
	if (isset($_GET['id'])){
		$userid =  (int)$_GET['id'];
		$sesId = Session::get("id");
		if ($userid != $sesId){
			header("Location: index.php");
		}
	}
	$user = new User();
	if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['updatepass'])) {
	 	$updatepass = $user->updatePassword($userid, $_POST);
	 }
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h2>Change Password<span class="pull-right"><a href="profile.php?id=<?php echo $userid; ?>" class="btn btn-primary">Back</a></span> </h2>
	</div>

	<div class="panel-body">
		<div style="max-width: 600px; margin: 0 auto;">
<?php 
	if (isset($updatepass)) {
		echo $updatepass;
	}
?>	
			<form action="" method="POST">
				<div class="form-group">
					<label for="old_pass">Old Password</label>
					<input type="password" name="old_pass" id="old_pass" class="form-control" placeholder="Enter Your Old Password">
				</div>

				<div class="form-group">
					<label for="password">New Password</label>
					<input type="password" name="password" id="password" class="form-control"  placeholder="Enter Your New Password">
				</div>

				<button class="btn btn-success" type="submit" name="updatepass">Update</button>	
			</form>
		</div>
	</div>
</div>
<?php
	include 'inc/footer.php';
?>
		